@ft:

Dans un fil ou on évoquait le savoir faire presque oublié du code inmaintenable
parce que beaucoup trop prêt de la machine, @hz me disait :
> Et je me dis que ça n'est pas pour rien que tu bricoles une 2600 🥰

(tl;dr : ce n'est pas parce que j'aime optimiser du code assembleur que je
programme pour 2600 mais l'inverse)

Ca m'a permis de prendre le temps de mettre des mots sur un truc qui m'est
apparu il y a quelque temps : c'est l'inverse. En fait programmer pour une de
ces vieilles machines en 2020, c'est avoir un univers très restreint, clos, qui
entre entièrement dans un cerveau, avec presque toute la doc disponible et des
passionnés pour échanger. C'est comme jouer au poker : on connait les cartes,
on connait les règles, on connait les maths, c'est uniquement à nous de trouver
ce qu'on veut/peut en faire. Il y a une espèce d'honnêteté dont l'absence dans
les systèmes actuels est liée, je ne sais pas trop comment, à une
procrastination maladive chez moi. J'ai d'ailleurs trouvé dans l'agilité
beaucoup (beaucoup) de choses liées au fait de luter contre cette même
procrastination, dont un texte de je ne sais plus qui (publié sur facebook,
peut peut être un des créateurs de scrum ? si quelqu’un a la ref je serais joie)
qui disait ne pas avoir de facilité pour les échanges sociaux et avoir voulu
créer avant tout un espace dans lequel les échanges soient clairs,
compréhensibles, sans pièges, pour pouvoir avancer et au final créer du
logiciel. C'est pour moi la meme chose que la notion de "peur" au cœur de
"TDD : by example". C'est le pendant technique de ceci que je trouve dans
l'informatique pré années 80 (et sur l'atari / amiga bien que je les connaisse
en fait très peu).

Bref, comme le disais déjà le tl;dr nécessaire : écrire du code automodifié
parce qu'on n'a pas le choix n'est pas un plaisir en soi pour moi, au pus un
casse-tête intéressant avec en bonus l'éventuelle satisfaction d'avoir poussé
la machine un peu plus loin que ce qu'on pensait possible. Par contre ca me
plonge dans un univers "sincère", dans lesquels on arrive à lire la
justification des contraintes. Même les choix les plus bizarres sur
l'architecture de l'atari 2600 sont explicables, là ou essayer de faire du
Springbootreactjs me donne l'impression de courir en slip dans le brouillard de
nuit en ayant dû me construire un bouclier je sais pas comment avec un baril de
lessive parce quelqu'un a fait le choix - je sais pas pourquoi - de cacher des
crocodiles sur le trajet que je dois emprunter de nuit pour bêtement aller
acheter du pain alors que la boulangerie est probablement fermée.

J'exagère un peu mais c'est parce que je trouvais la phrase jolie.

:cœur:3 :câlin:1 :crocodile:1 :bouclier:1


@pg:
Je n'oublierai pas cette métaphore de ci tôt.
Il faudrait une émoticône :slipbarilcrocodilepain: (modifié) 

:signe_d'addition_trait_plein:4 :sourire:1


@sr:
>avoir un univers très restreint, clos, qui entre entièrement dans un cerveau,
>avec presque toute la doc disponible et des passionnés pour échanger

+:100: pour moi, en plus j’ai une petite tête. Dans mon cas “ce qui rentre
entièrement” n’est pas très grand.

@pg:
J'aime également beaucoup l'image du poker. C'est un jeu que j'apprécie
particulièrement pour cette simplicité qui ouvre les portes vers une infinité
de situations

@hz:
Ca décrit tellement mon vécu sur tant de projets.
Actuellement, il y a un débat enflammé pour savoir s'il faut mettre des bonnets
en mohair aux crocodiles.
Le camp des contre ne voit pas bien l'intérêt.
Le camp des pour est consterné par ce manque d'engagement et demande aux contre
de monter un dossier pour expliquer comment ils vont aller chercher du pain
alors qu'il y a des crocodiles. Ils mettent en avant aussi le fait que la laine
mohair est un produit sain et naturel. Ils sont très agacés que les barils de
lessive ne soient pas vendus avec une pelote de laine. Je ne serais pas surpris
que ça se finisse par une lettre ouverte incendiaire aux fabricants de lessive.
(Au cas où vous posiez la question : je fais partie des contre. C'est des cache
nez qu'il faut.)

[...]

@hz:
Parfois ça donne envie d'aller voir ce qui se fait à côté... Les crocodiles du
voisin sont toujours plus verts

:crocodile:4 :rire_à_se_rouler_par_terre:2

@ft:
@hz, j'ai vraiment beaucoup trop rigolé pour que cette discussion soit
oubliée derriere un paywall de Slack. On ecrirait pas un talk pour le plaisir de
le voir refusé à Devoxx ?
